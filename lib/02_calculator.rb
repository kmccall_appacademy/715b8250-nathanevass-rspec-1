def add(a, b)
  a + b
end 

def subtract(a, b)
  a - b 
end 

def sum(arr)
  arr.empty? ? 0 : arr.reduce(:+)
end 

def multiply(arr)
  arr.reduce(:*)
end 

def power(base, power)
  base ** power
end 

def factorial(n)
  return 1 if n <= 1 
  n + factorial(n - 1)
end 