
  def to_pig(word)
    vowels = "aeiou".split("")
    split_chars = word.chars 
    split_chars.each.with_index do |char, i|
      if vowels.include?(char)
        word = word[1..-1] + char if word[-1] == "q"
        break 
      else 
        word = word[1..-1] + char 
      end 
    end 
    word + "ay"
  end 

  def capitalized?(word)
    word == word.capitalize
  end 


  def translate(phrase)
    phrase.split.map do |word| 
      capitalized?(word) ? to_pig(word.downcase).capitalize : to_pig(word)
    end.join(" ")
  end 
