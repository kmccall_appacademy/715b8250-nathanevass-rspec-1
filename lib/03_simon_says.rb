def echo(str)
    str
end 

def shout(str)
    str.upcase
end 

def repeat(str, times = 2)
    repeated_answer = ""
    (times - 1).times {repeated_answer += "#{str} "}
    repeated_answer + str 
end 

def start_of_word(word, letters)
    word[0...letters] 
end 

def first_word(str)
    str.slice(0, str.index(" "))
end 

def titleize(str)
    little_words = ["over", "the", "and"]
    str.split.map.with_index do |word, i|
        if i == 0 || !little_words.include?(word)
            word.capitalize 
        else 
            word 
        end 
    end.join(" ")
end 